import React from 'react';

import batman from "../../images/batman.png"

const ErrorPage = () => {
    return (
        <div className="quiz-bg">
            <div className="container">
                <h2>Oups, cette page n'existe pas</h2>
                <img src={batman} alt="error page"></img>
            </div>
        </div>
    )
}

export default ErrorPage

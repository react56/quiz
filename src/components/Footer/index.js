import React from 'react';
import '../../App.css';

const Footer = () => {
    return (
        <footer>
            <div className="footer footer-container">
                <p>Projet React 2020</p>
            </div>
        </footer>
    )
}

export default Footer

import React from 'react';
import { IconContext } from 'react-icons'
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import ErrorPage from '../ErrorPage';
import Footer from '../Footer';
import ForgetPassword from '../ForgetPassword';
import Header from '../Header';
import Landing from '../Landing';
import Login from '../Login';
import SignUp from '../SignUp';
import Welcome from '../Welcome';

import '../../App.css';

function App() {
  return (
    <BrowserRouter>
      <IconContext.Provider value={{ style: { verticalAlign: 'middle' } }}>
      <Header />

      <Switch>
        <Route exact path="/" component={Landing} />
        <Route path="/welcome" component={Welcome} />
        <Route path="/login" component={Login} />
        <Route path="/signup" component={SignUp} />
        <Route path="/forgetpassword" component={ForgetPassword} />
        <Route path="/" component={ErrorPage} />
      </Switch>

      <Footer />
      </IconContext.Provider>
    </BrowserRouter>
  );
}

export default App;
